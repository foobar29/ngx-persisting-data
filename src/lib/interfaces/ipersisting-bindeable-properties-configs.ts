import {StorageContext} from '../constants/enums/storage-context.enum';
import {IPersistingVariable} from './ipersisting-variable';

export interface IPersistingBindeablePropertiesConfigs {
  context?: StorageContext;
  namespace: string;
  properties: string[];
  component: object;
  onChange?: (elem: IPersistingVariable) => any;
}
