import {Subject} from 'rxjs';
import {IPersistingVariable} from './ipersisting-variable';
export interface IPersistingSubjects {
  [key: string]: Subject<IPersistingVariable>;
}
