export interface IPersistingVariable {
  key: string;
  value: any|any[];
}
