import {StorageContext} from '../constants/enums/storage-context.enum';
import {IPersistingVariable} from './ipersisting-variable';

export interface IPersistingBindeablePropertyConfigs {
  context?: StorageContext;
  namespace: string;
  property: string;
  component: object;
  onChange?: (elem: IPersistingVariable) => any;
}
