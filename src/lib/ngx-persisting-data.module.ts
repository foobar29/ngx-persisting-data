import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxPersistingData} from './ngx-persisting-data';



@NgModule({
  providers: [NgxPersistingData],
  bootstrap: [],
  exports: [],
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class NgxPersistingDataModule { }
export * from './ngx-persisting-data';
export * from './constants/enums/storage-context.enum';
