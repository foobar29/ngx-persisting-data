import {IPersistingKeys} from './interfaces/persisting-keys';
import {IPersistingDataConfigs} from './interfaces/ipersisting-data-configs';
import {StorageContext} from './constants/enums/storage-context.enum';
import {Observable, Subject} from 'rxjs';
import {IPersistingSubjects} from './interfaces/ipersisting-subjects';
import {IPersistingVariable} from './interfaces/ipersisting-variable';
import { IPersistingBindeablePropertiesConfigs } from './interfaces/ipersisting-bindeable-properties-configs';
import { IPersistingBindeablePropertyConfigs } from './interfaces/ipersisting-bindeable-property-configs';
import {Injectable} from '@angular/core';

/**
 * This library allows you to persist variables globally in your project
 *
 * @author Jesús Cantú
 * @date 11/02/2020
 */
@Injectable({providedIn: 'root'})
export class NgxPersistingData {
  private data: IPersistingKeys = {};
  private dataListeners: IPersistingSubjects = {};

  /**
   * Returns the current value of an existing item
   * @param namespace => the item's namespace
   * @param key       => item name
   * @param context   => storage context
   */
  public get = (key: string, namespace: string, context: StorageContext = StorageContext.Memory): any|any[] => {
    const keyName = this.getStructuredKeyName(key, namespace, context);
    let result;
    switch (context) {
      case StorageContext.Local: result = localStorage.getItem(keyName); break;
      case StorageContext.Session: result = sessionStorage.getItem(keyName); break;
      case StorageContext.Memory:
      default: result = this.data[keyName];
    }
    if (this.isValidJSON(result)) { result = JSON.parse(result); }
    return result;
  }

  /**
   * Returns an observable linked to an existing item
   * @param namespace => the item's namespace
   * @param key       => item name
   * @param context   => storage context
   */
    // tslint:disable-next-line:max-line-length
  public getAsObservable = (key: string, namespace: string, context: StorageContext = StorageContext.Memory): Observable<IPersistingVariable> => {
    const keyName = this.getStructuredKeyName(key, namespace, context);
    return this.dataListeners[keyName].asObservable();
  }

  /**
   * Sets a new item
   * @param conf => storage configs
   */
  public set = (conf: IPersistingDataConfigs): Observable<IPersistingVariable> => {
    conf.context = [undefined, null].includes(conf.context) ? StorageContext.Memory : conf.context;
    const keyName = this.getStructuredKeyName(conf.key, conf.namespace, conf.context);
    const currentValue = conf.value;
    if (typeof conf.value === 'object') { conf.value = JSON.stringify(conf.value); }
    switch (conf.context) {
      case StorageContext.Local: localStorage.setItem(keyName, conf.value); break;
      case StorageContext.Session: sessionStorage.setItem(keyName, conf.value); break;
      case StorageContext.Memory:
      default: this.data[keyName] = conf.value;
    }
    // tslint:disable-next-line:max-line-length
    if (![null, undefined].includes(this.dataListeners[keyName])) { this.dataListeners[keyName].next({key: conf.key, value: currentValue}); } else {
      this.dataListeners[keyName] = new Subject<any|any[]>();
      this.dataListeners[keyName].next({key: conf.key, value: this.get(conf.key, conf.namespace, conf.context)});
    }
    if (conf.expiration) {
      setTimeout(() => {
        this.remove(conf.namespace, conf.key, conf.context);
        if (conf.onExpiration) { conf.onExpiration({ key: conf.key, value: null }); }
      }, conf.expiration);
    }
    if (conf.onChange) { this.getAsObservable(conf.key, conf.namespace, conf.context).subscribe((elem) => conf.onChange(elem)); }
    return this.getAsObservable(conf.key, conf.namespace, conf.context);
  }

  /**
   * Removes the specified item
   * @param namespace => the item's namespace
   * @param key       => item name
   * @param context   => storage context
   */
  public remove = (key: string, namespace: string, context: StorageContext = StorageContext.Memory): void => {
    const keyName = this.getStructuredKeyName(key, namespace, context);
    switch (context) {
      case StorageContext.Local: localStorage.removeItem(keyName); break;
      case StorageContext.Session: sessionStorage.removeItem(keyName); break;
      case StorageContext.Memory: delete this.data[keyName];
    }
    if (this.dataListeners[keyName]) {
      this.dataListeners[keyName].next({key, value: null});
      delete this.dataListeners[keyName];
    }
  }

  /**
   * Removes the whole items in the specified storage context
   * @param context => storage context
   */
  public clear = (context: StorageContext = StorageContext.Memory): void => {
    switch (context) {
      case StorageContext.Local: localStorage.clear(); break;
      case StorageContext.Session: sessionStorage.clear(); break;
      case StorageContext.Memory: this.data = {};
    }
  }

  /**
   * Evaluates the existence of the specified item and executes a callback
   * @param namespace   => the item's namespace
   * @param key         => item name
   * @param context     => storage context
   * @param existing    => if exists callback
   * @param nonExisting => if doesn't exists callback
   */
    // tslint:disable-next-line:max-line-length
  public ifExists = (key: string, namespace: string, context: StorageContext, existing: (value?: any|any[]) => any, nonExisting?: () => any ): void => {
    if (this.get(key, namespace, context)) { existing(this.get(key, namespace, context)); } else if (nonExisting) { nonExisting(); }
  }

  /**
   * Overrides the get/set behavior of the specified property and binds it to the storage
   * @param conf  => property configs
   */
  public bindProperty(conf: IPersistingBindeablePropertyConfigs): Observable<IPersistingVariable> {
    conf.context = [undefined, null].includes(conf.context) ? StorageContext.Memory : conf.context;
    let value = this.get(conf.property, conf.namespace, conf.context);
    if ([null, undefined].includes(value)) { value = conf.component[conf.property]; }
    const dl: Observable<IPersistingVariable> = this.set({key: conf.property, namespace: conf.namespace, value, context: conf.context});
    Object.defineProperty(conf.component, conf.property, {
      enumerable: true,
      configurable: true,
      get: (): any => this.get(conf.property, conf.namespace, conf.context),
      // tslint:disable-next-line:no-shadowed-variable
      set: (value: any): any => this.set({key: conf.property, namespace: conf.namespace, value, context: conf.context})
    });
    if (conf.onChange) { this.getAsObservable(conf.property, conf.namespace, conf.context).subscribe((elem) => conf.onChange(elem)); }
    return dl;
  }

  /**
   * Overrides the get/set behavior of the specified properties and binds them to the storage
   * @param conf  => properties configs
   */
  public bindProperties = (conf: IPersistingBindeablePropertiesConfigs): void => {
    conf.context = [undefined, null].includes(conf.context) ? StorageContext.Memory : conf.context;
    // tslint:disable-next-line:max-line-length
    conf.properties.forEach((property) => this.bindProperty({property, context: conf.context, namespace: conf.namespace, component: conf.component, onChange: conf.onChange}));
  }

  /**
   * Checks if the string providen is a valid JSON
   * @param str => a serialized json
   */
  private isValidJSON = (str: string) => { try { JSON.parse(str); } catch (e) { return false; } return true; };

  /**
   * Retrieves a valid key name to store a variable
   * @param key       => item name
   * @param namespace => the item's namespace
   * @param context   => storage context
   */
  private getStructuredKeyName = (key: string, namespace: string, context: StorageContext): string => `${namespace}.${key}`;
}
