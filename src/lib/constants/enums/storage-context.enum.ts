export enum StorageContext {
  Local,
  Session,
  Memory
}
